$(function () {
        
$('[data-toggle="tooltip"]').tooltip();
$('[data-toggle="popover"]').popover();
$('.carousel').carousel({
  interval:2000
});

$("#confirmar-reserva").on("show.bs.modal", function(e) {
  console.log("La ventana Modal se esta Mostrando");
  $(".btn-reserva").removeClass("btn-primary");
  $(".btn-reserva").addClass("btn-secondary");
  $(".btn-reserva").prop("disabled",true);
});

$("#confirmar-reserva").on("shown.bs.modal", function(e) {
  console.log("La ventana Modal ya se Mostró");
});

$("#confirmar-reserva").on("hide.bs.modal", function(e) {
  console.log("La ventana Modal se esta Ocultando");
});

$("#confirmar-reserva").on("hidden.bs.modal", function(e) {
  console.log("La ventana Modal ya se Ocultó");
  $(".btn-reserva").prop("disabled",false);
  $(".btn-reserva").removeClass("btn-secondary");
  $(".btn-reserva").addClass("btn-primary");
  
  
});

})